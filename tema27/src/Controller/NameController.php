<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class NameController extends AbstractController
{
    /**
     * @Route("/name", name="name")
     */
    public function index()
    {
        return $this->render('name/index.html.twig', [
            'controller_name' => 'NameController',
        ]);
    }
}
