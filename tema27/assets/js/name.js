/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/name.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');
require('bootstrap');
import Vue from 'vue';

var name = new Vue({
    el:"#name",
    delimiters:['{*','*}'],
    data:{
        nume: 'Bidilean',
        prenume:'Simona',
        age:20,
        img:'img.jpg'
    },
    computed: {
        numeintreg: function(){
            return this.nume + " " + this.prenume;
        },
        multi: function(){
            return this.age * 3;
        },
        number: function(){
            return Math.floor(Math.random() * 101); 
        },
        name: function () {
            return this.nume + " " + this.prenume;
        }

    }
});
